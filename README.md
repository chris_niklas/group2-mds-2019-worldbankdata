# Group2-MDS-2019-WorldBankData

This is the group exercise from Working with Data course content.

Date 31-01-2019 to 02-11-2019

Data Source:

# open a connection to the world bank population health data
# https://console.cloud.google.com/marketplace/details/the-world-bank/global-health?filter=solution-type:dataset&filter=category:health&q=public%20data&id=f3c38e10-2c45-43c8-8a12-8d749ba987ee
con <- dbConnect(
    bigrquery::bigquery(),
    project = "bigquery-public-data",
    dataset = "world_bank_health_population",
    billing = gcp_project_id
)




Files - explanation

19-11-01-Group2-MiniProj...R - First script outside of R-Markdown

19-11-01-Group2-MiniProj-v1.RMD - continuation in markdown

