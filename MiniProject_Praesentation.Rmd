---
title: "Project 2 - World Bank Data - Education Spending on Mortality"
date: "11/1/2019"
output:
  ioslides_presentation: default
  beamer_presentation: default
subtitle: NOT MINI AT ALL! MDS-01-11-19
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

## IDEA:

## Dependent Vars:

* Mortality overall

* Mortality male per 1000 # (Mortality rate, adult, female (per 1,000 female adults)

* Mortality female per 1000 # Mortality rate, adult, male (per 1,000 male adults)



## Independent Vars:

* time 

* health expenditure per GDP 




## Preparation of Data Set
* Filter Vars
* drop unique identifier column
* reshape data set
* drop NA values
* rename columns

## Code
```{r tidy, echo = F} 
library(tidyverse)
library(tibble)
library(readr)
 


tbl_csv <- read.csv("tbl_wb_hnp.csv")

is.tibble(tbl)
tbl <- as.tibble(tbl_csv)
is.tibble(tbl)
```


```{r , include= T,  echo = F} 

tbl_f <- tbl%>%
  filter(
    indicator_name %in% c(
      "Mortality rate, adult, male (per 1,000 male adults)",
      "Mortality rate, adult, female (per 1,000 female adults)",
      "Capital health expenditure (% of GDP)"
    )
  )
```

## Code

```{r , include= T,  echo = F} 
tbl_ff <- tbl_f[-4]
tbl_fs <- spread(tbl_ff, key = indicator_name, value = value)
tbl_fs <- rename(tbl_fs, MortalityMale = "Mortality rate, adult, male (per 1,000 male adults)",MortalityFemale = "Mortality rate, adult, female (per 1,000 female adults)")
tbl_tidy <- drop_na(tbl_fs)
tbl_renamed <- tbl_tidy %>%
  rename("HealthExpenditure"="Capital health expenditure (% of GDP)")
```

## First output plot 

```  {r, include = T,  echo=F}
ggplot(data = tbl_renamed) +
aes(x=year,y=MortalityFemale,color=country_code) +
  geom_line() +
 theme(legend.position = "None")
```

## What to do now?

* Create 5 groups  of Capital Health Expenditure
* Summarise data per group 

## Code
```  {r, include = T, echo=F}
tbl_renamed <- tbl_renamed %>% 
  mutate(HealthExpenditureGroups = tbl_renamed$HealthExpenditure %>% 
           ntile(n=5))
tbl_renamed %>% group_by(HealthExpenditureGroups) %>%
  summarise(avg=mean(MortalityFemale)) 
```
## Second output plot
* Create Box Plot
* Compare Health Expenditure Groups with Female Mortality

## Code
```  {r, include = T, echo=F}

ggplot(data=tbl_renamed, aes(group = HealthExpenditureGroups, y=MortalityFemale)) +
geom_boxplot()

```